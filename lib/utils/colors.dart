import 'dart:ui';
class AppColors{
  static final Color textColor = const  Color(0xFFccc7c5);
  static final Color mainColor = const  Color(0xFFFAA961);
  static final Color iconColor1 = const  Color(0xFFBA633D);
  static final Color iconColor2 = const  Color(0xFFcc7c5);
  static final Color iconColor3 = const  Color(0xFFcc7c5);
  static final Color iconColor4 = const  Color(0xFFcc7c5);

}